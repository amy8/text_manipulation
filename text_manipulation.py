def count_men():
    count = 0;
    file = open('example_text.txt', encoding='utf-8')
    for line in file.readlines():
        for word in line.split():
            if word == "men":
                count += 1

    print("the word 'men' appears", count, "times")


def capitalize_women():
    file = open('example_text.txt', encoding='utf-8')
    output = open('example_text_new.txt', 'w', encoding='utf-8')
    for line in file.readlines():
        for word in line.split():
            if word.startswith("women") or word.startswith("Women"):
                output.write("WOMEN")
                output.write(word[5:])
            else:
                output.write(word)
            output.write(" ")
        output.write("\n")
    print("wrote new output to example_text_new.txt")


def contains_blue_devil():
    file = open('example_text.txt', encoding='utf-8')
    found = False
    for line in file.readlines():
        if line.find("Blue Devil") > 0:
            found = True
    if found:
        print("found Blue Devil in the file")
    else:
        print("did not find Blue Devil in the file")


if __name__ == "__main__":
    # DO SOMETHING
    pass
    f = open('example_text.txt', encoding='utf-8')
    print(f.readline())
    g = open('example_text.txt', encoding='utf-8')
    print(g.readlines())
    h = open('example_text.txt', encoding='utf-8')
    for x in h.readlines():
        print(x.rstrip())
    count_men()
    capitalize_women()
    contains_blue_devil()